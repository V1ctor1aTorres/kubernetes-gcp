terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "5.30.0"
    }
  }
}

provider "google" {
  # Configuration options
   project = "voltaic-spider-423823-c2"
   region = "us-central1"
   credentials = "key.json"
}